package course.labs.activitylab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ActivityOne extends Activity {


	private static final String RESTART_KEY = "restart";
	private static final String RESUME_KEY = "resume";
	private static final String START_KEY = "start";
	private static final String CREATE_KEY = "create";

	private final static String TAG = "Lab-ActivityOne";


    private int mCreate=0;
    private int mStart=0;
    private int mResume=0;
    private int mRestart=0;

    TextView mTvCreate,mTvRestart,mTvStart,mTvResume;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_one);

	
        mTvCreate=(TextView)findViewById(R.id.create);
        mTvStart=(TextView)findViewById(R.id.start);
        mTvRestart=(TextView)findViewById(R.id.restart);
        mTvResume=(TextView)findViewById(R.id.resume);


		Button launchActivityTwoButton = (Button) findViewById(R.id.bLaunchActivityTwo);
		launchActivityTwoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
		
				Intent intent = new Intent(getApplicationContext(),ActivityTwo.class);
				startActivity(intent);

			
			}
		});

		if (savedInstanceState != null) {

		
               mCreate= savedInstanceState.getInt(CREATE_KEY);
               mResume=savedInstanceState.getInt(RESUME_KEY);
               mStart=savedInstanceState.getInt(START_KEY);
               mRestart=savedInstanceState.getInt(RESTART_KEY);

		}

	
		Log.i(TAG, "Entered the onCreate() method");
        mCreate++;
        displayCounts();
	

	}



	@Override
	public void onStart() {
		super.onStart();
        mStart++;
        displayCounts();
	
		Log.i(TAG, "Entered the onStart() method");

	

	}

	@Override
	public void onResume() {
		super.onResume();
        mResume++;
        displayCounts();
	
		Log.i(TAG, "Entered the onResume() method");

	

	}

	@Override
	public void onPause() {
		super.onPause();

	
		Log.i(TAG, "Entered the onPause() method");
	}

	@Override
	public void onStop() {
		super.onStop();

	
		Log.i(TAG, "Entered the onStop() method");
	}

	@Override
	public void onRestart() {
		super.onRestart();

		// Emit LogCat message
		Log.i(TAG, "Entered the onRestart() method");

		mRestart++;
		displayCounts();
	

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	
		Log.i(TAG, "Entered the onDestroy() method");
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	
        savedInstanceState.putInt(CREATE_KEY, mCreate);
        savedInstanceState.putInt(RESTART_KEY, mRestart);
        savedInstanceState.putInt(START_KEY, mStart);
        savedInstanceState.putInt(RESUME_KEY, mResume);
	}


	public void displayCounts() {

	  

		mTvCreate.setText("onCreate() calls: " + mCreate);
		mTvStart.setText("onStart() calls: " + mStart);
		mTvResume.setText("onResume() calls: " + mResume);
		mTvRestart.setText("onRestart() calls: " + mRestart);

	}
}
